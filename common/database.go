package common

import "github.com/jinzhu/gorm"

var DB *gorm.DB

func InitMysql() (err error) {
	dsn := "root:123456@tcp(127.0.0.1:3306)/goStudy?charset=utf8mb4&parseTime=True&loc=Local"
	// 创建数据库
	DB, err = gorm.Open("mysql", dsn)
	if err != nil {
		return
	}
	// 连接数据库
	err = DB.DB().Ping()
	return err
}
