package controller

import (
	"github.com/gin-gonic/gin"
	"goBubble/dao"
	"goBubble/models"
	"net/http"
)

func IndexHandler(context *gin.Context) {
	context.HTML(http.StatusOK, "index.html", nil)
}
func AddTodo(c *gin.Context) {
	//	1. 从请求中把数据取出
	var todo models.Todo
	c.BindJSON(&todo)
	//	2. 存入数据库
	err := dao.AddTodo(&todo)
	if err != nil {
		//	3. 返回响应信息
		c.JSON(http.StatusOK, gin.H{
			"error": err.Error(),
		})
	} else {
		c.JSON(http.StatusOK, todo)
	}
}

func QueryTodoList(c *gin.Context) {
	todoList, err := dao.QueryTodoList()
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"error": err.Error,
		})
	} else {
		c.JSON(http.StatusOK, todoList)
	}
}

func UpdateTodo(c *gin.Context) {
	id, ok := c.Params.Get("id")
	if !ok {
		c.JSON(http.StatusOK, gin.H{
			"error": "id 必传",
		})
		return
	}
	var todo models.Todo
	_, err := dao.QueryATodo(id)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"error": err.Error(),
		})
	}
	c.BindJSON(&todo)
	err = dao.UpdateTodo(&todo)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"error": err.Error(),
		})
	} else {
		c.JSON(http.StatusOK, todo)
	}
}

func DeleteTodo(c *gin.Context) {
	id, ok := c.Params.Get("id")
	if !ok {
		c.JSON(http.StatusOK, gin.H{
			"error": "id 必传",
		})
		return
	}
	err := dao.DeleteTodo(id)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"error": err.Error(),
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"id": "deleted",
		})
	}
}
