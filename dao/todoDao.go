package dao

import (
	"goBubble/common"
	"goBubble/models"
)

func AddTodo(todo *models.Todo) (err error) {
	if err = common.DB.Create(&todo).Error; err != nil {
		return err
	}
	return
}

func QueryTodoList() (todoList []models.Todo, err error) {
	err = common.DB.Find(&todoList).Error
	return
}

func QueryATodo(id string) (todo models.Todo, err error) {
	err = common.DB.Where("id=?", id).First(&todo).Error
	return
}

func UpdateTodo(todo *models.Todo) (err error) {
	err = common.DB.Save(&todo).Error
	return err
}

func DeleteTodo(id string) (err error) {
	err = common.DB.Where("id=?", id).Delete(models.Todo{}).Error
	return
}
