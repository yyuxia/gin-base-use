package main

import (
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"goBubble/common"
	"goBubble/models"
	"goBubble/routes"
)

func main() {
	err := common.InitMysql()
	if err != nil {
		panic(err)
	}
	// 模型绑定
	common.DB.AutoMigrate(&models.Todo{})
	defer common.DB.Close() // 程序退出关闭数据库
	routes.SetupRouter()
}
