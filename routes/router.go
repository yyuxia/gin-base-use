package routes

import (
	"github.com/gin-gonic/gin"
	"goBubble/controller"
)

func SetupRouter() *gin.Engine {
	r := gin.Default()
	// 请求 /static 路由，在 web/static 目录下查找
	r.Static("/static", "web/static")
	r.Static("/public", "web/public")

	r.LoadHTMLGlob("web/pages/*")

	r.GET("/", controller.IndexHandler)

	// v1
	v1Group := r.Group("v1")
	{
		// 添加
		v1Group.POST("/todo", controller.AddTodo)
		// 查看
		// 查看所有
		v1Group.GET("/todo", controller.QueryTodoList)
		// 修改
		v1Group.PUT("/todo/:id", controller.UpdateTodo)
		//	删除
		v1Group.DELETE("/todo//:id", controller.DeleteTodo)
	}
	r.Run()
	return r
}
